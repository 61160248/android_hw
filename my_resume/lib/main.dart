import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Column _imgColumn(Color color, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      )
    ],
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget resumeSection = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Icon(
            Icons.people,
            color: Colors.black87,
          ),
          Text(
            ' ประวัติส่วนตัว',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ],
      ),
    );
    Widget resumeText = Container(
        padding: const EdgeInsets.only(right: 32, left: 32, bottom: 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('ชื่อ-นามสกุล : ณัฐฐาพร สิบอ่อน'),
            Text('เพศ : หญิง'),
            Text('วันเกิด : 24 มิถุนายน 2542:'),
            Text('อายุ :  22 ปี'),
            Text('สัญชาติ :  ไทย'),
          ],
        ));
    Widget edcSection = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Icon(
            Icons.cast_for_education,
            color: Colors.black87,
          ),
          Text(
            ' ประวัติการศึกษา',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ],
      ),
    );
    Widget edcText = Container(
      padding: const EdgeInsets.only(right: 32, left: 32, bottom: 0),
      child: Row(
        children: [
          Icon(
            Icons.star,
            color: Colors.pink[500],
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text(
                  "  ปริญญาตรี",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                '  Burapha University',
                style: TextStyle(color: Colors.grey[500]),
              )
            ],
          )),
        ],
      ),
    );
    Widget edcText2 = Container(
      padding: const EdgeInsets.only(right: 32, left: 32, bottom: 0),
      child: Row(
        children: [
          Icon(
            Icons.star,
            color: Colors.pink[500],
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text(
                  "  มัธยม",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                '  Sainampeung School',
                style: TextStyle(color: Colors.grey[500]),
              )
            ],
          )),
        ],
      ),
    );
    Widget skillSection = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Icon(
            Icons.safety_divider,
            color: Colors.black87,
          ),
          Text(
            ' ทักษะ',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ],
      ),
    );
    Widget skillText = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _imgColumn(color, 'VUE'),
          _imgColumn(color, 'HTML'),
          _imgColumn(color, 'CSS'),
          _imgColumn(color, 'JS'),
          _imgColumn(color, 'NodeJS'),
          _imgColumn(color, 'JAVA'),
        ],
      ),
    );
    Widget skillImg = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
         Image.asset(
              'images/vue.jpg',
              width: 40,
              height: 40,
          ),
          Image.asset(
              'images/html.jpg',
              width: 40,
              height: 40,
          ),
          Image.asset(
              'images/css.jpg',
              width: 40,
              height: 40,
          ),
          Image.asset(
              'images/js.jpg',
              width: 40,
              height: 40,
          ),
          Image.asset(
              'images/nodejs.jpg',
              width: 40,
              height: 40,
          ),
          Image.asset(
              'images/java.jpg',
              width: 40,
              height: 40,
          ),
        ],
      ),
    );
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My Resume Nutthaporn Sibon'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/me3.jpg',
              width: 250,
              height: 250,
              fit: BoxFit.scaleDown,
            ),
            resumeSection,
            resumeText,
            edcSection,
            edcText,
            edcText2,
            skillSection,
            skillImg,
            skillText
          ],
        ),
      ),
    );
  }
}
